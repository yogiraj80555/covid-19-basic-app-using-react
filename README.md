This is an Covid-19 Tracker created by me during exploring react and thanks for JavaScript Mastery for giving such simple guideline's of tutorial. it's a lot's appreciated.

The project is based on single page GUI which gives latest global count of corona cases along countrywise.
 
To See Running project <a href="https://yogiraj80555.gitlab.io/covid-19-basic-app-using-react">Click here</a> 

##Project View
<figure class="figure mb-3">
  <img src="https://www.patilyogiraj.ml/OtherWork/covid19BasicApp/1HomePage.JPG" width="650" height="380" class="figure-img img-fluid rounded" alt="User's Login Screen.">
  <figcaption class="figure-caption"><b><i>World Wide Global Cases</b></i></figcaption>
</figure>
<br/>
<figure class="figure mb-3">
  <img src="https://www.patilyogiraj.ml/OtherWork/covid19BasicApp/2HomePage.JPG" width="650" height="380" class="figure-img img-fluid rounded" alt="User's Login Screen.">
  <figcaption class="figure-caption"><b><i>Divide Data Chart Wise</b></i></figcaption>
</figure>

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.


To See Running project <a href="https://yogiraj80555.gitlab.io/covid-19-basic-app-using-react">Click here</a> 
<br/>
<figure class="figure mb-3">
  <img src="https://www.codewars.com/users/Error!!!!/badges/large"  class="figure-img img-fluid rounded" alt="User's Login Screen.">
  <figcaption class="figure-caption"><b><i>CodeWars Badges</b></i></figcaption>
</figure>
For any query, questions and suggestions please feel free to contact on below mail id.<br/>
Thankyou. <br/>
By Yogiraj Patil <br/>
Mail: yogiraj.218m0064@viit.ac.in
To Know <a href="http://www.yogiraj.ml">About me</a> 