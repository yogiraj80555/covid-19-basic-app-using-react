import React, { useState, useEffect} from 'react';
import { getDailyData } from '../../Api';

import { Line, Bar } from 'react-chartjs-2';

import styles from './Chart.module.css';

const Chart = ({data:{confirmed, recovered, deaths, country}}) => {
    const [dailyData, setDailyData ] = useState([]);

    useEffect ( () =>{
        const gettingData = async () => {
            setDailyData(await getDailyData()); 
        }
        //dailyData.length ? setDailyData(dailyData) : gettingData();
        
        gettingData();
        console.log(dailyData);
    },[setDailyData]/* without this runs infinitly, so now it will run only if `setFetchedCountries` is changed  */
    );


    const lineChart = (
        //sort if case if data avalible then else null
        dailyData.length ? (
            
        <Line
            data={{
                labels: dailyData.map(({date}) => date),
                datasets: [{
                    data: dailyData.map(({confirmed}) => confirmed),
                    label: 'Infected',
                    borderColor:  '#a22aaf',
                    fill: true,
                },
                {
                    data: dailyData.map(({deaths}) => deaths),
                    label: 'Deaths',
                    borderColor: 'red',
                    backgroundColor: 'rgba(200,0,0,0.5)',
                    fill: true, 
                }]
            }}
        />):<p><b><i>Sorry No Chart Avalible Now For Global Data</i></b></p>
    );
    

    const barChart = (
        confirmed?(
            <Bar 
                data = {{
                    labels: ['Infected', 'Recovered', 'Deaths'],
                    datasets: [{
                        label: 'people',
                        backgroundColor: [
                            'rgba(0, 0, 200, 0.8)',
                            'rgba(0, 200, 0, 0.5)',
                            'rgba(250, 0, 0, 0.5)'
                        ],
                        data:[confirmed.value, recovered.value, deaths.value]
                    }]
                 }}
                optoins={{
                    legend: { display: false },
                    title: { display: true, text:'Current state in ${country}'}
                }}
            />
        ):<p><b><i>Sorry No Chart Avalible Noe for Global Data  </i></b></p>
    );

    
    return (
        <div className={styles.container}>
            
            {//country ? barChar:lineChart
            }
            {barChart}
        </div>
    )
}

export default Chart;






















