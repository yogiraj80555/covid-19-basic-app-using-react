import React, { useState, useEffect} from 'react';
import { NativeSelect, FormControl } from '@material-ui/core';

import styles from './CountryPicker.module.css';

import { getCountryList } from '../../Api';

const CountryPicker = ({ handleChangeCountry }) => { //impliment handleChangeCountry  1.10.10 time 

    const [fetchedCountries, setFetchedCountries] = useState([]);

    useEffect( () => {
        const getCountries = async () => {
            setFetchedCountries( await getCountryList() );
        }

        //fetchedCountries.length ? setFetchedCountries(fetchedCountries):getCountries();
        getCountries(); 
    },[setFetchedCountries]/* without this runs infinitly, so now it will run only if `setFetchedCountries` is changed  */
    )

    return (
        <FormControl className={styles.formControl}>
            <NativeSelect defaultValue="" onChange={(e) => handleChangeCountry(e.target.value)}>
                <option value = "global">Global</option>
                {fetchedCountries.map((country, i) => <option key={i} value= {country}>{country}</option>)}
            </NativeSelect>
        </FormControl>
    )
}

export default CountryPicker; 