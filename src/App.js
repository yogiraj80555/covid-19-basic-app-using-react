import React from 'react';

//import Cards from './components/Cards/Cards';
//import Chart from './components/Chart/Chart';
//import CountryPicker from './components/CountryPicker/CountryPicker';

import {Cards, Chart, CountryPicker} from './components';

import styles from './App.module.css';

import { fetchData } from './Api'; 
import HeaderImage from './images/image.png';

class App extends React.Component{

    state = {
        confirmed:'',
        recovered:'',
        deaths:'',
        lastUpdate:'', 
        country:'',
    }


    async componentDidMount(){
       const {confirmed, recovered, deaths, lastUpdate} = await fetchData(); 

       this.setState({confirmed,recovered, deaths, lastUpdate })
    }

    handleChangeCountry = async (country) => {
        //console.log(country);
        let { confirmed, recovered,  deaths, lastUpdate } = await fetchData(country);
        this.setState({ confirmed, recovered,  deaths, lastUpdate, country});
        //console.log(this.state);

    }

     render(){
        
        let length = this.state.confirmed.length;
       
        if (length === 0){
            return <div className={styles.container}>
                Please wait......
            </div>
        }
            
         
         return(
             <div className={styles.container}>
                 
                 <img className={styles.image}   src={HeaderImage} alt="Yogiraj's Project"/>
                 <Cards data = {this.state} />
                 <CountryPicker handleChangeCountry = { this.handleChangeCountry } />
                 <Chart data={this.state} />
             </div>
         )
     }
}

export default App;