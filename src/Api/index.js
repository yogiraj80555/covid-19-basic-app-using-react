import axios from 'axios'


const url = "https://covid19.mathdro.id/api";


export const fetchData = async(country) => {
    var newUrl = url;
    if(country && country !== "global"){
        newUrl = `${url}/countries/${country}`
    }


    try{
        const response = await axios.get(newUrl);
        
        if(response.status === 200){return response.data;}
        //Note: handle exception when api is not working or not sending any data then what?
        return {confirmed:[], recovered:[], deaths:[]};
    }catch(error){
        console.log("Nothing it Busy");
        return {confirmed:[], recovered:[], deaths:[]};;
    }

}

export const getDailyData = async () => {
    try{
        //const datas1 = await axios.get(`${url}/daily`)
        /*if(datas.status === 200){
            console.log(datas)
            return datas.data.map((xyz) => ({
                confirmed: xyz.confirmed.total,
                deaths: xyz.deaths.total,
                date: xyz.reportDate,
            }));
           
        }
        return [];*/
        const getUserCountryCode = await axios.get("https://geolocation-db.com/json/697de680-a737-11ea-9820-af05f4014d91");
        var countryCode,countryName;
        if(getUserCountryCode.data.country_code === null){
            countryCode = "IN"
            countryName = "India"
        }
        else{
            countryCode = getUserCountryCode.data.country_code;
            countryName = getUserCountryCode.data.country_name;
        }
        const datas = await axios.get(`${url}/countries/${countryCode}`)
        if(datas.status === 200){
            //console.log(datas.data);
            //console.log("Length is "+datas.data.length)
            const pqr = {
                confirmed : datas.data.confirmed.value,
                deaths: datas.data.deaths.value,
                recovered: datas.data.recovered.value,
                date: datas.data.lastUpdate, 
                countryName: countryName,
                countryCode: countryCode,
            }
           // console.log(pqr);
            
        }
        
        return [];
    }catch(error){
        console.log("Daily Data API ERROR"+ error);
        return [];
    }
}


export const getCountryList = async () => {
    try{
        const response = await axios.get(`${url}/countries`);
        //console.log(response.data.countries);
        if(response.status === 200){
            return response.data.countries.map((country) => country.name);
        }

        //console.log(response);
        return [];
    }catch(error){
        console.log("countries error");
    }

}